import { lazy } from 'react';
import { RouteConfig } from 'react-router-config';

import AuthLayout from 'layouts/Auth';
import DashboardLayout from 'layouts/Dashboard';

const ROUTES = {
  ROOT: '/',
  LOGIN: '/login',
  RESET_PASSWORD: '/reset_password',
  HOME: '/dashboard',
};

const privateRouteList: RouteConfig[] = [
  {
    component: DashboardLayout,
    routes: [
      {
        path: ROUTES.ROOT,
        component: lazy(() => import('pages/RedirectToHome')),
        exact: true,
      },
      {
        path: ROUTES.HOME,
        component: lazy(() => import('pages/Home')),
        exact: true,
      },
      {
        path: ROUTES.LOGIN,
        component: lazy(() => import('pages/RedirectToHome')),
      },
      {
        path: ROUTES.RESET_PASSWORD,
        component: lazy(() => import('pages/RedirectToHome')),
      },
      {
        component: lazy(() => import('pages/NotFound')),
      },
    ],
  },
];

const publicRouteList: RouteConfig[] = [
  {
    component: AuthLayout,
    routes: [
      {
        path: ROUTES.RESET_PASSWORD,
        component: lazy(() => import('pages/ResetPassword')),
        exact: true,
      },
      {
        component: lazy(() => import('pages/Login')),
      },
    ],
  },
];

export { ROUTES, privateRouteList, publicRouteList };

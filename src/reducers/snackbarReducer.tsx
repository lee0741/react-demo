import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = '';

const snackbarSlice = createSlice({
  name: 'snackbar',
  initialState,
  reducers: {
    openSnackbar: (_state, action: PayloadAction<string>) => action.payload,
    closeSnackbar: () => '',
  },
});

const { actions, reducer } = snackbarSlice;

export const { openSnackbar, closeSnackbar } = actions;
export default reducer;

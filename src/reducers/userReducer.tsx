import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState = {
  uid: localStorage.getItem("react-admin-uid"),
  email: "",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    login: (state, _: PayloadAction<{ email: string; password: string }>) =>
      state,
    loginSuccess: (
      state,
      action: PayloadAction<{ uid: string; email: string }>
    ) => {
      state.uid = action.payload.uid;
      state.email = action.payload.email;
    },
    signout: (state) => state,
    signoutSuccess: (state) => {
      state.uid = null;
      state.email = "";
    },
    forgetPassword: (state, _: PayloadAction<{ email: string }>) => state,
    updatePassword: (state, _: PayloadAction<{ password: string }>) => state,
  },
});

const { actions, reducer } = userSlice;

export const {
  login,
  loginSuccess,
  signout,
  signoutSuccess,
  forgetPassword,
  updatePassword,
} = actions;
export default reducer;

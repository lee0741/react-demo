import { combineReducers } from '@reduxjs/toolkit';

import userReducer from './userReducer';
import snackBarReducer from './snackbarReducer';

const rootReducer = combineReducers({
  user: userReducer,
  snackbar: snackBarReducer,
});

export type State = ReturnType<typeof rootReducer>;

export default rootReducer;

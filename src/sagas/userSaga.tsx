import { PayloadAction } from "@reduxjs/toolkit";
import { all, call, fork, put, take, takeLatest } from "redux-saga/effects";

import {
  login,
  loginSuccess,
  signout,
  signoutSuccess,
  forgetPassword,
  updatePassword,
} from "reducers/userReducer";
import { openSnackbar } from "reducers/snackbarReducer";
import history from "utils/history";
import { ROUTES } from "routes";
import { auth } from "utils/firebase";

function* loginSaga(
  action: PayloadAction<{ email: string; password: string }>
) {
  const { email, password } = action.payload;

  try {
    const user = yield call(auth.signInWithEmailAndPassword, email, password);
    localStorage.setItem("react-demo-uid", user.user.uid);
  } catch (error) {
    yield put(openSnackbar(error.message));
  }
}

function* signoutSaga() {
  try {
    yield call(auth.signOut);
    yield put(signoutSuccess());
    localStorage.removeItem("react-demo-uid");
    history.push(ROUTES.LOGIN);
  } catch (error) {
    yield put(openSnackbar(error.message));
  }
}

function* getAuthSaga() {
  const channel = yield call(auth.channel);

  while (true) {
    const { user } = yield take(channel);

    if (user) {
      yield put(loginSuccess({ uid: user.uid, email: user.email }));
    } else {
      localStorage.removeItem("react-demo-uid");
      history.push(ROUTES.LOGIN);
    }
  }
}

function* forgetPasswordSaga(action: PayloadAction<{ email: string }>) {
  try {
    yield call(auth.sendPasswordResetEmail as any, action.payload.email);
    history.push(ROUTES.LOGIN);
    yield put(openSnackbar("Reset Email has been sent."));
  } catch (error) {
    yield put(openSnackbar(error.message));
  }
}

function* updatePasswordSaga(action: PayloadAction<{ password: string }>) {
  try {
    yield call(auth.updatePassword, action.payload.password);
    yield put(openSnackbar("Password has been updated"));
  } catch (error) {
    yield put(openSnackbar(error.message));
  }
}

export default function* userSaga() {
  yield fork(getAuthSaga);
  yield all([
    takeLatest(login, loginSaga),
    takeLatest(signout, signoutSaga),
    takeLatest(forgetPassword, forgetPasswordSaga),
    takeLatest(updatePassword, updatePasswordSaga),
  ]);
}

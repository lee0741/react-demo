import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import ReduxSagaFirebase from "redux-saga-firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBT4muoYgsQOre8CbyoQfq0lmfq3iynocg",
  authDomain: "react-demo-be983.firebaseapp.com",
  projectId: "react-demo-be983",
  storageBucket: "react-demo-be983.appspot.com",
  messagingSenderId: "118109573110",
  appId: "1:118109573110:web:851e2377a2eb7ed6b181d7",
};

const app = firebase.initializeApp(firebaseConfig);

const rsf = new ReduxSagaFirebase(app);

export const { auth, firestore } = rsf;

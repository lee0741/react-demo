export const capitalize = (input: string) =>
  input ? input[0].toUpperCase() + input.slice(1) : '';

import React from 'react';
import { connect } from 'react-redux';
import { Router } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import ErrorBoundary from 'pages/ErrorBoundary';
import { publicRouteList, privateRouteList } from 'routes';
import { closeSnackbar } from 'reducers/snackbarReducer';
import { State } from 'reducers';
import { ScrollReset } from 'components';
import history from 'utils/history';

const Unauthenticated = () => renderRoutes(publicRouteList);

const Authenticated = () => renderRoutes(privateRouteList);

const App: React.FC = ({ uid, role, open, message, handleClose }: any) => {
  return (
    <Router history={history}>
      <ErrorBoundary>
        <ScrollReset />
        {uid ? <Authenticated /> : <Unauthenticated />}
        <Snackbar
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          message={<span>{message}</span>}
          action={
            <IconButton color="inherit" onClick={handleClose}>
              <CloseIcon />
            </IconButton>
          }
        />
      </ErrorBoundary>
    </Router>
  );
};

const mapStateToProps = (state: State) => ({
  uid: state.user.uid,
  open: Boolean(state.snackbar),
  message: state.snackbar,
});

const mapDispatchToProps = {
  handleClose: () => closeSnackbar(),
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

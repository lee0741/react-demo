import React from 'react';
import { FieldProps } from 'formik';
import MuiTextField from '@material-ui/core/TextField';

interface Props extends FieldProps {
  label: string;
  type?: string;
}

const TextField = ({ field, form, label, type }: Props) => {
  const { errors, touched } = form;
  const { value, name } = field;

  return (
    <MuiTextField
      variant="outlined"
      margin="normal"
      fullWidth
      error={Boolean(errors[name] && touched[name])}
      helperText={errors[name] && touched[name] ? errors[name] : null}
      label={label}
      type={type ? type : 'text'}
      {...field}
      value={value}
    />
  );
};

export default TextField;

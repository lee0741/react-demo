import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Page } from 'components';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
}));

const DashboardAnalytics = () => {
  const classes = useStyles();

  return (
    <Page className={classes.root} title="Dashboard">
      <Typography>Home</Typography>
    </Page>
  );
};

export default DashboardAnalytics;

import React from 'react';
import { connect } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { Formik, Form, FastField } from 'formik';
import * as yup from 'yup';
import { Avatar, Container, Typography, Link } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';

import { Page, TextField, SubmitButton } from 'components/';
import { login } from 'reducers/userReducer';
import { ROUTES } from 'routes';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
}));

const initialValues = {
  email: '',
  password: '',
};

interface Props {
  login: (values: typeof initialValues) => void;
}

const Login = ({ login }: Props) => {
  const classes = useStyles();

  const schema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required(),
  });

  return (
    <Page title="Login">
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <Formik
            initialValues={initialValues}
            validationSchema={schema}
            onSubmit={login}>
            <Form className={classes.form}>
              <FastField label="Email" name="email" component={TextField} />
              <FastField
                label="Password"
                type="password"
                name="password"
                component={TextField}
              />
              <Link
                component={RouterLink}
                to={ROUTES.RESET_PASSWORD}
                variant="body2">
                Forget Password?
              </Link>
              <SubmitButton>Login</SubmitButton>
            </Form>
          </Formik>
        </div>
      </Container>
    </Page>
  );
};

const mapDispatchToProps = {
  login,
};

export default connect(null, mapDispatchToProps)(Login);

import React from 'react';
import { Redirect } from 'react-router';

import { ROUTES } from 'routes';

const RedirectToHome = () => <Redirect to={ROUTES.HOME} />;

export default RedirectToHome;

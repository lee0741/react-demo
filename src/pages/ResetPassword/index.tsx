import React from 'react';
import { connect } from 'react-redux';
import { Formik, Form, FastField } from 'formik';
import * as yup from 'yup';
import { Avatar, Container, Typography } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';

import { SubmitButton, TextField, Page } from 'components';
import { forgetPassword } from 'reducers/userReducer';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const initialValues = {
  email: '',
};

interface Props {
  forgetPassword: (values: typeof initialValues) => void;
}

const ResetPassword = ({ forgetPassword }: Props) => {
  const classes = useStyles();

  const schema = yup.object().shape({
    email: yup.string().email().required(),
  });

  return (
    <Page title="Reset Password">
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Reset Password
          </Typography>
          <Formik
            initialValues={initialValues}
            validationSchema={schema}
            onSubmit={forgetPassword}>
            <Form className={classes.form}>
              <FastField label="Email" name="email" component={TextField} />
              <SubmitButton>Reset</SubmitButton>
            </Form>
          </Formik>
        </div>
      </Container>
    </Page>
  );
};

const mapDispatchToProps = {
  forgetPassword,
};

export default connect(null, mapDispatchToProps)(ResetPassword);

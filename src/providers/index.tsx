import React from 'react';
import { Provider } from 'react-redux';

import ThemeProvider from 'providers/themeProvider';
import store from 'store';

const AppProvider: React.FC = ({ children }) => (
  <Provider store={store}>
    <ThemeProvider>{children}</ThemeProvider>
  </Provider>
);

export default AppProvider;

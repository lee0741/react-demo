import React from 'react';
import { ThemeProvider as MuiProvider, makeStyles } from '@material-ui/styles';

import theme from 'theme';

const useStyles = makeStyles({
  '@global': {
    '*': {
      boxSizing: 'border-box',
      margin: 0,
      padding: 0,
    },
    html: {
      height: '100%',
      '-webkit-font-smoothing': 'antialiased',
      '-moz-osx-font-smoothing': 'grayscale',
    },
    body: {
      backgroundColor: '#f4f6f8',
      height: '100%',
    },
    a: {
      textDecoration: 'none',
    },
    '#root': {
      height: '100%',
    },
  },
});

const ThemeProvider: React.FC = ({ children }) => {
  useStyles();
  return <MuiProvider theme={theme}>{children}</MuiProvider>;
};

export default ThemeProvider;

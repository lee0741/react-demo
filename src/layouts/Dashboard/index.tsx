import React, { Suspense, useState } from 'react';
import { connect } from 'react-redux';
import { renderRoutes, RouteConfigComponentProps } from 'react-router-config';
import { makeStyles } from '@material-ui/core/styles';
import { LinearProgress } from '@material-ui/core';

import NavBar from './NavBar';
import TopBar from './TopBar';
import { State } from 'reducers';

const useStyles = makeStyles({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
  },
  topBar: {
    zIndex: 2,
    position: 'relative',
  },
  container: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
  },
  navBar: {
    zIndex: 3,
    width: 256,
    minWidth: 256,
    flex: '0 0 auto',
  },
  content: {
    overflowY: 'auto',
    flex: '1 1 auto',
  },
});

interface Props extends RouteConfigComponentProps {
  email: string;
}

const DashboardLayout = ({ email, route }: Props) => {
  const classes = useStyles();
  const [openNavBarMobile, setOpenNavBarMobile] = useState(false);

  const handleNavBarMobileOpen = () => {
    setOpenNavBarMobile(true);
  };

  const handleNavBarMobileClose = () => {
    setOpenNavBarMobile(false);
  };

  return (
    <div className={classes.root}>
      <TopBar
        className={classes.topBar}
        onOpenNavBarMobile={handleNavBarMobileOpen}
      />
      <div className={classes.container}>
        <NavBar
          className={classes.navBar}
          onMobileClose={handleNavBarMobileClose}
          openMobile={openNavBarMobile}
          email={email}
        />
        <main className={classes.content}>
          <Suspense fallback={<LinearProgress />}>
            {route ? renderRoutes(route.routes) : null}
          </Suspense>
        </main>
      </div>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  email: state.user.email,
});

export default connect(mapStateToProps)(DashboardLayout);

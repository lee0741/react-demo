import React from "react";
import { connect } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Hidden,
  Typography,
} from "@material-ui/core";
import InputIcon from "@material-ui/icons/Input";
import MenuIcon from "@material-ui/icons/Menu";

import { signout } from "reducers/userReducer";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "none",
  },
  flexGrow: {
    flexGrow: 1,
  },
  logoutButton: {
    marginLeft: theme.spacing(1),
  },
  logoutIcon: {
    marginRight: theme.spacing(1),
  },
  title: {
    color: "#fff",
  },
  link: {
    textDecoration: "none",
  },
}));

interface Props {
  className: string;
  onOpenNavBarMobile: () => void;
  handleSignout: () => void;
}

const TopBar = ({ className, onOpenNavBarMobile, handleSignout }: Props) => {
  const classes = useStyles();

  return (
    <AppBar className={clsx(classes.root, className)} color="primary">
      <Toolbar>
        <RouterLink to="/" className={classes.link}>
          <Typography
            component="h1"
            variant="h4"
            noWrap
            className={classes.title}
          >
            React Demo
          </Typography>
        </RouterLink>
        <div className={classes.flexGrow} />
        <Hidden mdDown>
          <Button
            className={classes.logoutButton}
            color="inherit"
            onClick={handleSignout}
          >
            <InputIcon className={classes.logoutIcon} />
            Logout
          </Button>
        </Hidden>
        <Hidden lgUp>
          <IconButton color="inherit" onClick={onOpenNavBarMobile}>
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

const mapDispatchToProps = {
  handleSignout: () => signout(),
};

export default connect(null, mapDispatchToProps)(TopBar);

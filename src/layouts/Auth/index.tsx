import React, { Suspense } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { LinearProgress } from '@material-ui/core';

import Topbar from './Topbar';
import { renderRoutes, RouteConfigComponentProps } from 'react-router-config';

const useStyles = makeStyles((theme) => ({
  content: {
    height: '100%',
    paddingTop: 56,
    [theme.breakpoints.up('sm')]: {
      paddingTop: 64,
    },
  },
}));

const AuthLayout = ({ route }: RouteConfigComponentProps) => {
  const classes = useStyles();

  return (
    <>
      <Topbar />
      <main className={classes.content}>
        <Suspense fallback={<LinearProgress />}>
          {route ? renderRoutes(route.routes) : null}
        </Suspense>
      </main>
    </>
  );
};

export default AuthLayout;
